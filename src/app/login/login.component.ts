import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { initializeApp } from '@firebase/app';
import { getAuth, onAuthStateChanged, User, RecaptchaVerifier,AuthProvider } from 'firebase/auth';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  windowRef: any = {};
  constructor(public afAuth: AngularFireAuth) { }
  user: any = null;
  ngOnInit(): void {
    debugger;
    console.log(environment.app);
    this.afAuth.authState.subscribe(user => {
      if (user) {
        console.log(user);
        this.user = user
        localStorage.setItem('user', JSON.stringify(this.user));
      } else {
       
      }
    });
    this.windowRef.recaptchaVerifier = new RecaptchaVerifier('sign-in-button', {
      'size': 'invisible',
      'callback': (response:any) => {
        // reCAPTCHA solved, allow signInWithPhoneNumber.
        console.log(response)
      }
    }, getAuth(initializeApp(environment.firebase)));
  }
  sendOpt() {
    if (!this.otpSent) {
      this.afAuth.signInWithPhoneNumber("+91" + this.phone, this.windowRef.recaptchaVerifier).then(x => {
        this.windowRef.confirmationResult = x;
        this.otpSent = true;
      });
    } else {
      this.verifyOtp();
    }
  }
  code = "";
  phone = "";
  otpSent = false;
  verifyOtp() {
   this.windowRef.confirmationResult.confirm(this.code).then((result:any) => {
      // User signed in successfully.
     const user = result.user;
     console.log(user);
      // ...
    }).catch((error:any) => {
     
    });
  }
}

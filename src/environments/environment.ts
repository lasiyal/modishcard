// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import { FirebaseApp } from "@angular/fire/app";

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAFJk6ttiLuOxz_ugkqTMiI3QV0Sw1u-fQ",
    authDomain: "modish-card.firebaseapp.com",
    projectId: "modish-card",
    storageBucket: "modish-card.appspot.com",
    messagingSenderId: "500081194616",
    appId: "1:500081194616:web:044c73007c1ea0666b1f8d",
    measurementId: "G-9RKHX06D6R"
  },
  app: {} as FirebaseApp
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
